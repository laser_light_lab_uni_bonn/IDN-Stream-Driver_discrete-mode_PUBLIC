//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//
// Copyright (c) 2014-2023, University of Bonn, Institute of Computer Science 4.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the University of Bonn nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
// AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the University of Bonn.
//
// Contacts:    Dr. Matthias Frank (matthew@cs.uni-bonn.de)
//              Andreas Binczek    (laser-light-lab@uni-bonn.de)
//              Daniel Schröder    (laser-light-lab@uni-bonn.de)
//              Daniel Schwenzfeur (laser-light-lab@uni-bonn.de)
//              Spartak Ehrlich    (laser-light-lab@uni-bonn.de)
//
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


#ifndef IDN_IDN_API_H_INCLUDED
#define IDN_IDN_API_H_INCLUDED

#include "idn_api_dev.h"



//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//
// Packed Header
//
#ifndef PACK_INCLUDED
	#define PACK_INCLUDED
	#ifdef _WIN32
		#define BEGIN_PACK __pragma( pack(push, 1) )
		#define END_PACK __pragma( pack(pop) )
	#else
		#define BEGIN_PACK 
		#define END_PACK __attribute__((__packed__))
	#endif
#endif


// Data Format Frame
//   A laser frame for output is an array of the following 8 byte structure
//
//   (identical to Data Format Frame - Easylase API)
//		X, Y can be configured to be 16 bit signed or 12 bit unsigned
//
//		12 bit: only use value range 0, ..., 4095 in unsigned short (16 bit)
//		16 bit: full value range -32768, ..., 0, ..., +32767 in short (16 bit)
//
typedef struct
{
    unsigned short x;  // 2 Bytes  Value 12/16 bit X-Coordinate
    unsigned short y;  // 2 Bytes  Value 12/16 bit Y-coordinate
    unsigned char  r;  // 1 Byte   Value 0 - 255   Red
    unsigned char  g;  // 1 Byte   Value 0 - 255   Green
    unsigned char  b;  // 1 Byte   Value 0 - 255   Blue
    unsigned char  i;  // 1 Byte   Value 0 - 255   Intensity
}IDNLasePoint;



//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//
// IDNlib Error Codes
// 
//
#define IDNLASE_ERR_NOT_INITIALIZED (int)-1
#define IDNLASE_ERR_BAD_INDEX       (int)-2
#define IDNLASE_ERR_BAD_PARAMETER   (int)-3
#define IDNLASE_ERR_CARDSTATUS      (int)-4     // card busy, USB error, ...


//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//
// IDNlib API
//



// init dictionary 8 Byte, three colors R,G,B

IDNLIB_EXPORT(void)  IDNInitDict8_RGB();

// init dictionary six colors R,G,B + 3 additional

IDNLIB_EXPORT(void)  IDNInitDict_RGB_6Colors();


// default X,Y 16 bit signed => needs function to change to 12 bit unsigned
//			(EasyLase API, MediaLas MLD API)

IDNLIB_EXPORT(void)  IDNSetXY12bitUnsigned();


// same as function EasyLaseGetCardNum:integer;register;
IDNLIB_EXPORT(int)  IDNLaseGetCardNum();

// same as function EasyLaseStop( var CardNumber:integer ):boolean;stdcall;
IDNLIB_EXPORT(int)  IDNLaseStop(int cardNumber);

// same as function EasyLaseClose():boolean;stdcall;
IDNLIB_EXPORT(int)  IDNLaseClose();


#define IDNLASE_GET_STATUS_USBERROR    0x00 // custom IDN
#define IDNLASE_GET_STATUS_READY       0x01
#define IDNLASE_GET_STATUS_BUSY        0x02

// same as function EasyLaseGetStatus( var CardNumber:integer ):integer;stdcall;
IDNLIB_EXPORT(int)  IDNLaseGetStatus(int cardNumber);

// definements not needed ...
//#define IDNLASE_MIN_SPEED 		    500
//#define IDNLASE_MAX_SPEED 		    250000
//#define IDNLASE_SPEED_STEP		    500

// same as Deplhi function EasyLaseWriteFrame( var CardNumber:integer; DataBuffer:pointer; DataCounter:integer; Speed:word ):boolean;stdcall;
// dataCounter is amount of bytes for the frame, i.e. 8 Byte * number of points in frame
IDNLIB_EXPORT(int)  IDNLaseWriteFrame(int cardNumber, unsigned char *dataBuffer,
                                      unsigned int dataCounter, unsigned int speed);

// identical to IDNLaseWriteFrame, i.e. "NR" as in EasyLase API is not implemented
IDNLIB_EXPORT(int)  IDNLaseWriteFrameNR(int cardNumber, unsigned char *dataBuffer,
                                      unsigned int dataCounter, unsigned int speed);

// same as function EasyLaseWriteDMX( var CardNumber:integer; DMXBuffer:pointer ):boolean;stdcall;
IDNLIB_EXPORT(int) IDNLaseWriteDMX(int cardNumber, unsigned char *dmxBuffer);

// same as function EasyLaseDMXOut( var CardNumber:integer; DMXBuffer:pointer; Baseaddress:word; ChannelCount:word ):boolean;stdcall;
IDNLIB_EXPORT(int) IDNLaseDMXOut(int cardNumber, unsigned char *dmxBuffer, unsigned short baseAddress, unsigned char channelCount);

// tells whether the device "cardNumber" offers IDN DMX512 service
// return 0 == DMX not available
// return 1 == DMX output available
IDNLIB_EXPORT(int) IDNLaseGetDMXStatus(int cardNumber);

// get name of the device, "IDN."+"<serialnumber>"+"<channelName>"
// cardName is pointer to buffer created by caller
// size is the size of the buffer in Bytes
IDNLIB_EXPORT(int)  IDNLaseGetName(int cardNumber, char* cardName, unsigned int size);

// get friendly name of the device (format maybe configured in idn.conf)
// cardName is pointer to buffer created by caller
// size is the size of the buffer in Bytes
IDNLIB_EXPORT(int)  IDNLaseGetFriendlyName(int cardNumber, char* cardName, unsigned int size);

#endif // IDN_IDN_API_H_INCLUDED
