# November 5, 2013 - ten years of IDN since the first ever IDN laser show replay !!!

At the 25th ILDA (International Laser Display Association) conference in Aalen (Germany), the first ever laser show was replayed directly from laser show software (Medialas MIII) with the ILDA Digital Network (IDN) sent to an IDN converter (prototype of the StageMate ISP, DexLogic Karlsruhe) and a laser projector connected to it.

Historical video material can be found at:
[https://youtube.com/@laser-light-lab-uni-bonn]

You can read more in English on [Facebook](https://www.facebook.com/matthias.frank.1612147/posts/pfbid0Msjc6nR8cCHXDex9mcPP7zNExbm1cjSX1ipdZKFmqPN6uRPcdL4VoWuUJtuH3mwol) or in German on [LinkedIn](https://www.linkedin.com/posts/matthias-frank-6a2575272_5-november-2013-vor-genau-10-jahren-activity-7127014914019209216-nKPr/)


# IDN-Stream-Driver_discrete-mode_PUBLIC

This repository contains the portable / binary version of our **IDN-Stream-Driver** (discrete graphics mode, aka frame mode of the **ILDA Digital Network**) which allows existing laser software to use the API and pass over laser data (X, Y coordinates and colors R,G,B) on a frame by frame basis, which will be send over the local network to IDN capable consumers.

The inspiration for this project goes back to the API of the old Easylase.dll and the Medials MLD API (which actually are still provided by the current IDN.dll and IDN_64.dll).

The development of this software started in 2013 in student projects at the University of Bonn in the course of our activities on the ILDA Digital Network for the International Laser Display Association (ILDA).

The source code will be published later in a separate repository. The current development environment is based on GNU gcc and MinGW-w64 and compiles the library for Linux and Windows (DLL in both 32 and 64 bit). Compiling for Apple Mac OS has not been tested, but might work as well.


# Intended Audience of this Repository:

If you have programming experience with laser show software and have already implemented interfacing e.g. to old EasyLase.dll, Etherdream.dll, Helios_DAC.dll or similar laser controller APIs, then using the IDN.dll should not be a problem (and the documentation given below should be good to go).

## Quick Start for Developers using the IDN API / Windows DLL

To get the IDN.dll running with your Windows software, you may follow some or all of the following steps:

### 1. play around with the DllTester-IDN.exe

Download the folders /32bit and/or /64bit and start DllTester-IDN.exe by double-click and follow commandline instructions in the terminal window.

Use it with IDN capable consumers **which implement the service discovery of IDN-Hello** [https://www.ilda.com/technical.htm -> Discovery Protocol "IDN-Hello"], e.g. the Dexlogic ISP StageMate or our visualization tool **IDN-Toolbox**:

IDN-Toolbox HowTo Video: https://www.youtube.com/watch?v=Jj_wTgCwtMA

IDN-Toolbox portable versions: https://uni-bonn.sciebo.de/s/NyOy2lMwThRpwXZ

The DLLTester-IDN.exe will create laser test output as a square with RGBW colored edges or a circle with RGB color gradient.

The coordinate range of square/circle is hardcoded to 0...4095. Therefore, the size of the projected image depends on the selected mode of 12 bit unsigned integer vs. 16 bit signed integer for X,Y coordinates.

### 2. look at idn_api.h

The file idn_api.h contains all data structures, definements and function signatures (with some sparse comments) needed to use the IDN API.

You may use idn_api.h in your project to link to the IDN.dll. Alternatively, the following paragraphs show how the DllTester-IDN.exe is using the IDN.dll without the .h file.

### 3. look at the C++ sources of DllTester-IDN.exe

The sources of DllTester-IDN.exe are in the folder DllTester-IDN_Source and can be compiled with MinGW-w64 or Microsoft Visual Studio. The provided **DllTester-IDN.vcxproj** has recently been used with Microsoft Visual Studio 2017.

The DllTester-IDN.exe is loading the IDN.dll/IDN_64.dll dynamically and does not need the idn_api.h. The main.cpp uses Typedefs and GetProcAddress() for all necessary IDN functions of the IDN API.

### 4. further notes

- Using 12 bits **resolution for X, Y coordinates** is historically coming from the Easylase API/Medialas MLD. We recommend to use the IDN-Stream-Driver in 16 bit mode.

- **Colors R,G,B** are currently in 8 bit resolution. Offering a mode of 16 bit colors is subject for further work (but not limited by the IDN Stream Specification, which generally allows to add precision in steps of 8 bit, i.e. 16 bit, 24 bit, ...).

- **Support of six colors** is also subject for further work. The function IDNInitDict_RGB_6Colors() already visible in idn_api.h is not yet implemented.

- The IDN API also offers **functions for DMX**. These are not yet covered by the DllTester-IDN.exe but have been tested earlier with Medialas software (e.g. MIII) via the MLD API.

- Several parameters of the IDN-Stream-Driver maybe (re-)configured by a **configuration file** which needs to be placed in the same folder as the IDN.dll. Without such conf file all parameters use pre-coded default values. Further documentation on this will follow later.

## References

- As of August 31, 2023, the IDN.dll has been added to the implementation of the **LFI Player 3D Laser Display Software** (V.1.3.2).
[https://sourceforge.net/projects/lfiplayer3d/]

- The IDN.dll is working with all **Medialas software** (32 bit, via MLD API), such as Mamba Black, Mamba 2.0, MIII, and Mamba X4 (the dll just needs to have a file ending .mld in the program folder, e.g. rename to IDN.dll.mld).
The first demonstration of a prototype version of the IDN-Stream-Driver with playback of laser shows from Medialas software has been presented at the annual ILDA conference 2013 in Aalen, Germany.

- The IDN.dll is included in the installer of **HE Laserscan** software since version 7.1.f dated 14.11.2018
[https://he-laserscan.de/]

- The IDN.dll is working with **Dynamics**, as also has been demonstrated at the annual ILDA conferences (e.g. live laser jockey participation of Uni Bonn with Dynamics Live). A version working with IDN is available upon request from the Dynamics publisher Guido Jaeger.
[https://www.mylaserpage.de/]

- The IDN.dll is also working with **LSX (LaserShow Xpress)** via the Easylase API (dll needs to be renamed to EasyLase.dll in the program folder).
[http://innolasers.com/ebay/laser_diode/LSX.php] 
However, it may have some restrictions on timing of some IDN function calls. If you encounter problems in using LSX with the IDN.dll, get in touch to receive further assistance.


## License

When using the binaries of IDN.dll / IDN_64.dll in your project please adhere to the "Revised BSD License" (3-clause-license). The license text can be found in the files [idn_LICENSE.txt](/idn_LICENSE.txt), [IDN.dll_LICENSE.txt](/32bit/IDN.dll_LICENSE.txt) and [IDN_64.dll_LICENSE.txt](/64bit/IDN_64.dll_LICENSE.txt) in this repository.


## Project History

TBD.

## Credits

The following students contributed to this project, in the order of most recent contributions first:

- Spartak Ehrlich (2022, 2023)
- Daniel Schwenzfeur
- Daniel Schröder
- Andreas Binczek

In a previous project the student Simon Ofner created our first prototype of the IDN-Stream-Driver in continous graphics mode (aka wave mode) of the ILDA Digital Network. First public presentations of this software were given in the year 2013 at the combined LaserFreak meeting and the annual ILDA conference in Aalen, Germany.


## Feedback

We would be happy to receive feedback on your experience using our IDN tools on your side. You may send just comments, or also screenshots, screen capture video, or similar. 

Just drop an e-mail to laser-light-lab@uni-bonn.de


# **DISCLAIMER**

Please be aware that the IDN Stream Specification is an open protocol, like e.g. IP, the Internet Protocol, and its documentation is freely available to everybody (download at ILDA Technical Standards, https://www.ilda.com/technical.htm ). "_The IDN-Stream standard describes the encoding of laser show artwork into digital data streams._"

The specification and also manifold hardware and software elements have been implemented by volunteers, who are interested and motivated to foster the development of the open and publicly available components of the ILDA Digital Network. See more at http://ilda-digital.com

Up to now, no effort has been spent into encrypting or authenticating the IDN stream - as maybe known from other media types. 

When using our IDN tools, the copyrights of the artists who have created laser shows or laser show content have to be respected, as well as possible license agreements of the laser show system that is used together with our IDN tools.

**The University of Bonn / the Laser & Light Lab is not liable for damages or copyright/license infringements coming from third person's or third parties' use of the IDN prototype software provided via these gitlab.com groups or project repositories.**
