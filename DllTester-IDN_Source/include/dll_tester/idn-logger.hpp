/*-----------------------------------------------------------
IDN-Audio-Controller
	
File: idn-logger.hpp

Author:
	- Spartak Ehrlich

Contents: 
    - Idn Logger Definition + Declaration  
    ==> wraps std::cout with a PrintThread so print calls are not time sensetive 
    ==> uses LogLevels (Info, Warning, Error, Critical, Debug) with formatted and Coloured output 
    ==> uses form :'DD-MM-YYYY HH-MM-SS [ModN][LogLevel]    Message' ; where ModN = ModuleName (Should be 4 Chars)

MISC:
    - Fold Expression Method needs to be in this Header file!

    - Don't make a static Logger ==> Problems with static Logger (will be destroyed after return from main())!
    ==> join is still correct (no corruption)
-----------------------------------------------------------*/
#ifndef _IDN_LOGGER_HPP
#define _IDN_LOGGER_HPP

#include <condition_variable>
#include <iostream>
#include <sstream>
#include <queue>
#include <mutex>
#include <thread>

namespace idn{

enum class LogLevel{
    rset = 0, // can be used to print out like default std::cout (without color/time/newline)
    info = 1, 
    warn = 2,
    crit = 3, 
    erro = 4,
    debg = 5, 
};

class Logger{
    friend class LoggerThread;
private:
    std::string moduleName;
    static std::mutex mtxLock;
    static std::condition_variable condVar;
    static std::queue<std::string> printQueue; 

    void printTimeStamp(std::ostringstream& buffer);
    void changeColour(std::ostringstream& buffer, LogLevel lvl);

public:
    Logger();
    Logger(std::string moduleName);
    ~Logger();

    /** Logger Message Function
     * 
     *  Buffered stdout print Function to minimize latency from buffered io
     * 
     * @param lvl Level of message (Info, Warning, Critical, Error, Debug, reset)
     * @param args Cout like template (unlimited params as a method call seperated by ','
    */
    template<typename... Args> 
    void printMessage(LogLevel lvl, Args... args);
};

// Needs to be in the Header file otherwise the fold expression does not work
template<typename... Args> 
void Logger::printMessage(LogLevel lvl, Args... args){
    std::ostringstream buffer;

    if(lvl != LogLevel::rset){
        printTimeStamp(buffer);
        changeColour(buffer, lvl);
        (buffer << ... << args); 
        changeColour(buffer, LogLevel::rset);
    }else
        (buffer << ... << args);
    
    mtxLock.lock();
    printQueue.push(buffer.str());
    mtxLock.unlock();
    condVar.notify_all();
}

}
#endif