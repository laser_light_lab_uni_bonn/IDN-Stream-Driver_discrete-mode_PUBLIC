#include <iostream>
#include <thread>

// to use M_PI, older Visual Studio 2017 needs <math.h>
#define _USE_MATH_DEFINES
#include <math.h>
// recent versions are fine with <cmath>, e.g. mingw-w64
// https://qastack.com.de/programming/1727881/how-to-use-the-pi-constant-in-c
// possibly both includes work ?!?
#include <cmath>

#include <windows.h>

//#include "idn_api.h" // Not needed!
#include "../../include/dll_tester/idn-logger.hpp"
 
using namespace std;
using namespace idn;

// Typedefs for method signatures
typedef void (WINAPI *IDNInitDict8_RGBType)();
typedef void (WINAPI *IDNSetXY12bitUnsignedType)(); 
typedef int (WINAPI *IDNLaseGetCardNumType)(); 
typedef int (WINAPI *IDNLaseStopType)(int cardNumber); 
typedef int (WINAPI *IDNLaseCloseType)();
typedef int (WINAPI *IDNLaseWriteFrameType)(int cardNumber, unsigned char* databuffer, unsigned int dataCounter, unsigned int speed);
typedef int (WINAPI *IDNLaseGetNameType)(int cardnumber, char* cardName, unsigned int size);
typedef int (WINAPI *IDNLaseGetFriendlyNameType)(int cardNumber, char* cardName, unsigned int size);

// Global variables for methods
HMODULE streamDriverDLL = nullptr;
IDNInitDict8_RGBType IDNInitDict8_RGB= nullptr;
IDNSetXY12bitUnsignedType IDNSetXY12bitUnsigned = nullptr;
IDNLaseGetCardNumType IDNLaseGetCardNum = nullptr;
IDNLaseStopType IDNLaseStop = nullptr;
IDNLaseCloseType IDNLaseClose = nullptr;
IDNLaseWriteFrameType IDNLaseWriteFrame = nullptr;
IDNLaseGetNameType IDNLaseGetName = nullptr;
IDNLaseGetFriendlyNameType IDNLaseGetFriendlyName= nullptr;

// global var. for thread
bool running;

// Assumption: compiler alignment for stack is correct (no padding!); 16bit alignment, so probably fine
typedef struct{ 
    unsigned short x, y; 
    unsigned char r, g, b, i;
} IDNLasePoint;

// Method declarations
int loadIdnDll();
void generateSquarePoints(IDNLasePoint* frame);
void generateCirclePoints(IDNLasePoint* frame);
void startIdnStream();
void sendIdnPacket(int input, IDNLasePoint* frame, void (*generatePicture)(IDNLasePoint*));

// 'Main' routine
int main(){
    Logger logger{"DLLTESTER"};

    if(loadIdnDll()){
        logger.printMessage(LogLevel::crit, "Could not load Methods from IDN DLL");
        logger.printMessage(LogLevel::crit, "Expected names: 32Bit|64Bit; IDN.dll|IDN_64.dll");
        return 1;
    }
    logger.printMessage(LogLevel::info, "Loaded DLL successfully!");

    int input;
    string inputStr;

    do{
        logger.printMessage(LogLevel::info, "Input '0' to start 12bit unsigned integer mode");
        logger.printMessage(LogLevel::info, "Input '1' to start 16bit signed integer mode");
        logger.printMessage(LogLevel::info, "Input '2' to terminate the process");

        logger.printMessage(LogLevel::rset, ">> ");
        cin >> inputStr;
        
        try{
            input = stoi(inputStr);
        }catch(invalid_argument &error){
            input = -1;
        }

        switch(input){
            case 0:
                IDNInitDict8_RGB();
                IDNSetXY12bitUnsigned();
                startIdnStream();
                break; 
            case 1:
                IDNInitDict8_RGB();
                startIdnStream();
                break;
            default:
                break;
        }

        
    }while(input != 2);

    logger.printMessage(LogLevel::info, "Terminating Program");
    return 0;
}

// Load dll + methods entries 
int loadIdnDll() {
    if(sizeof(void*) == 8) streamDriverDLL = LoadLibrary(TEXT("IDN_64.dll"));
    else streamDriverDLL = LoadLibrary(TEXT("IDN.dll"));

    if(streamDriverDLL == nullptr) return 1;

    IDNInitDict8_RGB = (IDNInitDict8_RGBType) GetProcAddress(streamDriverDLL, "IDNInitDict8_RGB");
    IDNSetXY12bitUnsigned = (IDNSetXY12bitUnsignedType) GetProcAddress(streamDriverDLL, "IDNSetXY12bitUnsigned");
    IDNLaseGetCardNum = (IDNLaseGetCardNumType) GetProcAddress(streamDriverDLL, "IDNLaseGetCardNum");
    IDNLaseStop = (IDNLaseStopType) GetProcAddress(streamDriverDLL, "IDNLaseStop");
    IDNLaseClose = (IDNLaseCloseType) GetProcAddress(streamDriverDLL, "IDNLaseClose");
    IDNLaseWriteFrame = (IDNLaseWriteFrameType) GetProcAddress(streamDriverDLL, "IDNLaseWriteFrame");
    IDNLaseGetName = (IDNLaseGetNameType) GetProcAddress(streamDriverDLL, "IDNLaseGetName");
    IDNLaseGetFriendlyName = (IDNLaseGetFriendlyNameType) GetProcAddress(streamDriverDLL, "IDNLaseGetFriendlyName");

	if(!IDNInitDict8_RGB || !IDNSetXY12bitUnsigned || !IDNLaseGetCardNum || !IDNLaseStop || !IDNLaseClose || !IDNLaseWriteFrame || !IDNLaseGetName ||!IDNLaseGetFriendlyName)
        return 2;

    return 0;
}

// Generate a square with 0,0 | 0,4095 | 4095,4095 | 4095,0 borders, with exactly 400 points (!)
// The lines are white, red, blue and green
void generateSquarePoints(IDNLasePoint* frame){
    if(!frame)
        return;

    for(int i = 0; i < 100; i++){
        frame[i].x = 0; 
        frame[i].y = (i == 99 ? 4095 : i * 41);
        frame[i].r = 255; 
        frame[i].g = 255;
        frame[i].b = 255;
    }

    for(int i = 100; i < 200; i++){
        frame[i].x = (i == 199 ? 4095 : (i - 100) * 41); 
        frame[i].y = 4095;
        frame[i].r = 255; 
        frame[i].g = 0;
        frame[i].b = 0;
    }

    for(int i = 200; i < 300; i++){
        frame[i].x = 4095; 
        frame[i].y = (i == 299 ? 0 : 4095 - (i - 200) * 41);
        frame[i].r = 0; 
        frame[i].g = 255;
        frame[i].b = 0;
    }

    for(int i = 300; i < 400; i++){
        frame[i].x = (i == 399 ? 0 : 4095 - (i - 300) * 41);
        frame[i].y = 0;
        frame[i].r = 0; 
        frame[i].g = 0;
        frame[i].b = 255;
    }

    return;
}

// Generate a circle with 'fade' effect, with exactly 400 points (!)
// copied + ported from 'Project-IDN-FrameMode'
//      ==> was lazy, so only 0-4095@16Bit ==> small circle ==> no need to change anything for 12bit upscale
void generateCirclePoints(IDNLasePoint* frame){ 
    if(!frame)
        return;

    int i;
    double p = 0;

    for(i = 0; i < 400; i++){
	    frame[i].x = (int) (sin(p) * 2047) + 2047;
	    frame[i].y = (int) (cos(p) * 2047) + 2047;
	    frame[i].r = (unsigned char) (sin(p + M_PI / 3.0) * 127.5 + 127.5);
	    frame[i].g = (unsigned char) (sin(p + M_PI * 2 / 3.0) * 127.5 + 127.5);
	    frame[i].b = (unsigned char) (sin(p + M_PI) * 127.5 + 127.5);
	    p += M_PI * 2 / 400;
    }

    return;
}

// Do a service discovery, select target + picture + send frames to consumer
void startIdnStream(){
    Logger logger{"DLLTESTER"};
    logger.printMessage(LogLevel::info, "Doing a service discovery....");
    int rv = IDNLaseGetCardNum();

    if(rv == 0){
        logger.printMessage(LogLevel::warn, "Did not find any IDN devices. Returning to method menu.");
        return;
    }

    logger.printMessage(LogLevel::info, "Got '", rv, " 'respones: ");

    for(int i = 0; i < rv; i++){
        char name[80];
        char fName[80];

        IDNLaseGetName(i, name, sizeof(name));
        IDNLaseGetFriendlyName(i, fName, sizeof(fName));

        logger.printMessage(LogLevel::info, "   >> Index i '", i, "' | Name '", name, "' | Friendly name '", fName, "'");
    }

    int targetInput, pictureInput;

    logger.printMessage(LogLevel::info, "Choose target via index: ");
    logger.printMessage(LogLevel::rset, ">> ");
    cin >> targetInput; 

    if(targetInput < 0 || targetInput > rv - 1){
        logger.printMessage(LogLevel::erro, "Invalid input. Returning to method menu.");
        return;
    }

    logger.printMessage(LogLevel::info, "Got two different testing pictures: ");
    logger.printMessage(LogLevel::info, ">> Index 0 'Square'");
    logger.printMessage(LogLevel::info, ">> Index 1 'Circle'");

    logger.printMessage(LogLevel::info, "Choose picture via index: ");
    logger.printMessage(LogLevel::rset, ">> ");
    cin >> pictureInput; 

    if(pictureInput < 0 || pictureInput > 1){
        logger.printMessage(LogLevel::erro, "Invalid input. Returning to method menu.");
        return;
    }

    void (*generatePicture)(IDNLasePoint*);

    if(!pictureInput){
        generatePicture = &generateSquarePoints;
    }else{
        generatePicture = &generateCirclePoints;
    }

    IDNLasePoint* frame = new IDNLasePoint[400];
    if(frame == nullptr){
        logger.printMessage(LogLevel::erro, "Could not allocate enough memory! Check 'malloc' or system ram.");
        return;
    }

    logger.printMessage(LogLevel::info, "Press 'Enter' to stop sending....");

    running = true;
    thread callbackThread (sendIdnPacket, targetInput, frame, generatePicture); // thread because i'm lazy

    cin.get();
    cin.get();

    running = false;
    callbackThread.join();
    delete[] frame;
    IDNLaseStop(targetInput);
    IDNLaseClose();
}

// Thread which will send a packet every second until user input 
void sendIdnPacket(int input, IDNLasePoint* frame, void (*generatePicture)(IDNLasePoint*)){
    Logger logger{"DLLTESTER"};

    for(int i = 0; running ; i++){
        generatePicture(frame);			// hard coded for 400 points per frame
        logger.printMessage(LogLevel::debg, "Generating picture with scanrate '",  10000 + (i*10000 % 60000), "'");
        IDNLaseWriteFrame(input, (unsigned char*) frame, sizeof(IDNLasePoint) * 400, 10000 + (i*10000 % 60000)); // method destroys 'frame' data ==> will be filled with new data every next call
        Sleep(1000);
    }
}