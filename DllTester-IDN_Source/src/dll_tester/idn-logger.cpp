/*-----------------------------------------------------------
IDN-Audio-Controller
	
File: idn-logger.cpp

Author:
	- Spartak Ehrlich

Contents: 
	- Logger Implementation
    ==> uses a LoggerThread (static Class) which prints the Messages by watching the queue

MISC:
    - LoggerThread Class is in .cpp so the user can't reach it 
-----------------------------------------------------------*/
#define _CRT_SECURE_NO_WARNINGS
#include <condition_variable>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <mutex>
#include <queue>
#include <sstream>
#include <thread>

#include "../../include/dll_tester/idn-logger.hpp"


using namespace std;

// ANSI Colour Codes
#define COLOUR_RESET   "\x1b[0m" 
#define COLOUR_RED     "\x1b[31m"
#define COLOUR_GREEN   "\x1b[32m"
#define COLOUR_YELLOW  "\x1b[33m"
#define COLOUR_BLUE    "\x1b[34m"
#define COLOUR_MAGENTA "\x1b[35m"
#define COLOUR_CYAN    "\x1b[36m"
#define COLOUR_WHITE   "\x1b[37m"

namespace idn{

class LoggerThread{
private:
    static std::unique_ptr<std::thread> printThread;
    static std::queue<std::string> printQueueCopy;  
    static bool isRunning;
    static bool skip;

    LoggerThread();
    ~LoggerThread();

    static void threadEntry();

    #ifdef IDN_WINDOWS
        static void setupConsole();
    #endif
public:
    static void startPrintThread();

    LoggerThread(LoggerThread const&) = delete;
    void operator=(LoggerThread const&) = delete;
};

/*      Logger        */

// Static Vars. initialisation 
mutex Logger::mtxLock;
queue<string> Logger::printQueue;
condition_variable Logger::condVar;

Logger::Logger(){
    Logger("UNKW");
}

// Sets the ModuleName 
Logger::Logger(std::string moduleName){
    this->moduleName = '[' + moduleName + ']';
    LoggerThread::startPrintThread();
}

Logger::~Logger(){}

// adds the timestamp into the ostringstream
void Logger::printTimeStamp(std::ostringstream& buffer){
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);

    buffer << std::put_time(&tm, "%d-%m-%Y %H:%M:%S ");
}

// Changes the Colour + appends the ModuleName + LogLevel
void Logger::changeColour(std::ostringstream& buffer, LogLevel lvl){
    switch(lvl){
        case LogLevel::info:
            buffer << COLOUR_GREEN << moduleName << "[Info]     ";
            break;
        case LogLevel::warn:
            buffer << COLOUR_YELLOW << moduleName << "[Warning]  ";
            break;
        case LogLevel::crit:
            buffer << COLOUR_MAGENTA << moduleName << "[Critical] ";
            break;
        case LogLevel::erro:
            buffer << COLOUR_RED << moduleName << "[Error]    ";
            break;
        case LogLevel::debg:
            buffer << COLOUR_CYAN << moduleName << "[DEBUG]    ";
            break;
        default:
            buffer << COLOUR_RESET << '\n';
            break;
    }
}

/*      LoggerThread        */
// Static Vars. initialisation 
unique_ptr<thread> LoggerThread::printThread = nullptr;
queue<string> LoggerThread::printQueueCopy;
bool LoggerThread::isRunning = false;
bool LoggerThread::skip = false;


// Start LoggingThread if there is none yet
LoggerThread::LoggerThread(){
    isRunning = true;
    printThread = unique_ptr<thread>(new thread(&LoggerThread::threadEntry));
    if(!printThread)
        isRunning = false;
}

// Terminate LoggingThread
LoggerThread::~LoggerThread(){
    isRunning = false;
    Logger::condVar.notify_all();
    if(printThread)
        printThread->join();
}

#ifdef IDN_WINDOWS
#include <winsock2.h>
#include <windows.h>

// Set the Windows Console to enable ANSI + UTF-8
void LoggerThread::setupConsole(){
    DWORD outMode;
    HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
    GetConsoleMode(console, &outMode);
    outMode |= 0x0004;

    SetConsoleTitle(TEXT("DLLTester-IDN Ver.0.9"));
    SetConsoleMode(console, outMode);
    SetConsoleOutputCP(65001);
}

#endif

// Thread Callback Method, where the printing to std::cout is done
void LoggerThread::threadEntry(){
    #ifdef IDN_WINDOWS
        setupConsole();
    #endif
    mutex m;
    unique_lock<mutex> lck(m);

    do{
        Logger::mtxLock.lock();
        if(Logger::printQueue.size()) // Because we could miss a cond_notify when we are still printing to cout!
            skip = true;
        else
            skip = false;
        Logger::mtxLock.unlock();

        if(!skip)
            Logger::condVar.wait(lck);

        Logger::mtxLock.lock();
        while(Logger::printQueue.size()){ // Copy the whole queue before printing otherwise we would not safe time
            printQueueCopy.push(Logger::printQueue.front());
            Logger::printQueue.pop();
        }
        Logger::mtxLock.unlock();

        while(printQueueCopy.size()){
            cout << printQueueCopy.front(); 
            printQueueCopy.pop();
        }
    }while(isRunning);
}

// Inits the Thread by creating a static variable of it
void LoggerThread::startPrintThread(){
    static LoggerThread instance; 
}

}